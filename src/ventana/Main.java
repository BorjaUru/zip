package ventana;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class Main extends JFrame {
  private JButton b;
  private JButton    b1;
  private JFrame  f;
  private JPanel  p;
  private JTextField tf;
  private JTextField tf1;
  public Main() {
    tf = new JTextField();
    tf1 = new JTextField();
    tf1.setPreferredSize(new Dimension(200, 25));
    tf.setPreferredSize(new Dimension(200, 25));
    f = this;
    b = new JButton("Examinar");
    b1 = new JButton("Crear");
    b1.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent e) {
        if (!tf.getText().equals("") && new File(tf.getText()).exists()) {
          JFileChooser fc = new JFileChooser();
          try {
            if (fc.showSaveDialog(null) == fc.APPROVE_OPTION) {
              crear(tf.getText(), fc.getSelectedFile().getAbsolutePath());
            }
          } catch (Exception ex) {
            ex.printStackTrace();
          }

        } else {
          JOptionPane.showMessageDialog(f, "Directorio no seleccionado o no existe", "Error", JOptionPane.ERROR_MESSAGE);
        }

      }
    });
    b.addActionListener(new ActionListener() {

      @Override
      public void actionPerformed(ActionEvent e) {
        JFileChooser fc = new JFileChooser();
        fc.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
        fc.showOpenDialog(f);
        tf.setText(fc.getSelectedFile().toString());

      }
    });
    p = new JPanel();
    p.add(tf);
    p.add(b);
    p.add(b1);
    setContentPane(p);
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    pack();
    setVisible(true);
  }

  public void crear(String dir, String nombre) throws IOException {
    FileOutputStream fos = new FileOutputStream(nombre + ".zip");
    ZipOutputStream zipOut = new ZipOutputStream(fos);
    if (new File(dir).isDirectory()) {
      recorrerDirecto(dir, zipOut);
    } else {
      addFiche(new File(dir), zipOut);
    }
    zipOut.close();
    fos.close();

  }

  public void recorrerDirecto(String dir, ZipOutputStream zipOut) throws IOException {
    for (File f : new File(dir).listFiles()) {
      if (f.isDirectory()) {
        recorrerDirecto(f.getAbsolutePath(), zipOut);
      } else {
      addFiche(f, zipOut);
      }
    }

  }

  public void addFiche(File f, ZipOutputStream zipOut) throws IOException {
    // https://trellat.es/crear-un-fichero-de-un-tamano-en-windows/
    FileInputStream fis = new FileInputStream(f);
    ZipEntry zipEntry = new ZipEntry(f.getName());
    zipOut.putNextEntry(zipEntry);
    final byte[] bytes = new byte[1024];
    int length;
    while ((length = fis.read(bytes)) >= 0) {
      zipOut.write(bytes, 0, length);
    }
    fis.close();
  }

  public static void main(String[] args) {
    new Main();
  }

}
